var Papa = require('papaparse')

Array.prototype.minBy = function(fn) { return this.extremumBy(fn, Math.min); };

Array.prototype.maxBy = function(fn) { return this.extremumBy(fn, Math.max); };

Array.prototype.extremumBy = function(pluck, extremum) {
  return this.reduce(function(best, next) {
    var pair = [ pluck(next), next ];
    if (!best) {
       return pair;
    } else if (extremum.apply(null, [ best[0], pair[0] ]) == best[0]) {
       return best;
    } else {
       return pair;
    }
  },null)[1];
}

module.exports = class Sorter {
  constructor() {
    this.box = document.getElementById('drag-box')
    this.box.addEventListener('dragover', () => this.handle_drag_over(event), false)
    this.box.addEventListener('drop', () => this.handle_file_select(event), false)
  }

  handle_drag_over(event) {
    event.stopPropagation()
    event.preventDefault()

    event.dataTransfer.dropEffect = 'link'
    return
  }

  handle_file_select(event) {
    event.stopPropagation()
    event.preventDefault()

    var loaded = false
    var file = event.dataTransfer.files[0]

    this.parse_csv(file)
    return
  }

  parse_csv(file, callback) {
    Papa.parse(file, {header: true, complete: (results) => this.handle_data(results.data)})
  }

  handle_data(data) {
    var players = []
    for (var row of data) {
      if (row["Submission Date"] != "") {
        var player = {
          name: row["First Name"].trim().toLowerCase() + " " + row["Last Name"].trim().toLowerCase(),
          skill: parseInt(row["In order to help us select fair teams, please rank your basketball skill (we think everyone is great at basketball!):"]),
          teammate_request: row["Teams are \"randomly drafted\" by Effie and Aerienne. You are allowed to choose *one* friend that you would like to be on the same team with -- we will try to accomodate your request!"]
        }
        players.push(player)
      }
    }

    this.pick_teams(players)
  }

  pick_teams(data) {
    function team_value(team) {
      return team.map((a) => a.skill).reduce((a, b) => a + b, 0)
    }

    var teams = []
    teams[0] = []
    teams[1] = []
    teams[2] = []
    teams[3] = []

    var sorted_by_skill = [...data.entries()].sort((a,b) => a[1].skill > b[1].skill ? -1 : a[1].skill < b[1].skill ? 1 : 0)

    for (var datum of sorted_by_skill) {
      var player = datum[1]
      var lowest_value_team = teams.minBy((a) => team_value(a))
      var team_with_request = teams.find((a) => a.some((e) => e.name == player.teammate_request.trim().toLowerCase()))

      if (team_with_request == undefined) {
        lowest_value_team.push(player)
      }
      else {
        console.log('request filled! ' + player.name + ' requested ' + player.teammate_request)
        team_with_request.push(player)
      }
    }

    var iterator = 1
    for (var team of teams) {
      var team_div = document.createElement("div")
      team_div.className = "team-container"

      var team_title_div = document.createElement("div")
      team_title_div.className = "team"
      team_title_div.innerHTML = "Team " + iterator

      this.box.appendChild(team_div)
      team_div.appendChild(team_title_div)

      for (var player of team) {
        var player_div = document.createElement("div")
        player_div.className = "player"
        player_div.innerHTML = player.name
        team_div.appendChild(player_div)
      }

      var score_div = document.createElement("div")
      score_div.className = "score"
      score_div.innerHTML = team_value(team)
      team_div.appendChild(score_div)
      iterator += 1
    }
  }
}
